import styled from "styled-components";
import { ButtonRetangle } from "./Button/CalButton";
import { ButtonSquare } from "./Button/CalButton";
import Style from "../App.module.css";
const Calculator = () =>{
    return (
        <>
            <div className="container mt-4">
                <div className={Style.All} >
                 <div className="container">
                    <div className="mt-3" style={{width : "400px" , height : "180px" , backgroundColor : "#dfe0e6"}}>

                    </div>
                    <div className="mt-3">
                        <div>
                            <ButtonRetangle fColor >DEL</ButtonRetangle>
                            <ButtonSquare fColor  fSize = "10px" bgColor = "#dddee4">{'<-'}</ButtonSquare>
                            <ButtonSquare fColor bgColor = "#b9d7d5">/</ButtonSquare>
                        </div>
                        <div>
                            <ButtonSquare bgColor = "#e5e4ea">7</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">8</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">9</ButtonSquare>
                            <ButtonSquare fColor bgColor = "#b9d7d5">*</ButtonSquare>
                        </div>
                        <div>
                            <ButtonSquare bgColor = "#e5e4ea">4</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">5</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">6</ButtonSquare>
                            <ButtonSquare fColor bgColor = "#b9d7d5">-</ButtonSquare>
                        </div>
                        <div>
                            <ButtonSquare bgColor = "#e5e4ea">1</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">2</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">3</ButtonSquare>
                            <ButtonSquare fColor bgColor = "#b9d7d5">+</ButtonSquare>
                        </div>
                        <div>
                            <ButtonSquare bgColor = "#e5e4ea">0</ButtonSquare>
                            <ButtonSquare bgColor = "#e5e4ea">,</ButtonSquare>
                            <ButtonRetangle bgColor>=</ButtonRetangle>
                        </div>
                    </div>

                 </div>
                </div>
            </div>
        </>
    )
}

export default Calculator