import styled from "styled-components";

const ButtonSquare = styled.button`
    width : 100px;
    height : 100px;
    background-color : ${props => props.bgColor};
    border : 2px solid white;
    font-size : 20px;
    color : ${props  => props.fColor ? "#606060" : "#7caaab"}

`
const ButtonRetangle = styled.button`
    width : 200px;
    height : 100px;
    background-color : ${props => props.bgColor ? "#549a96" : "#dddee4"};
    border : 2px solid white;
    font-size : 21px;
    color : ${props => props.fColor ? "#848587" : "#f4f9f8"}
`
export {ButtonSquare , ButtonRetangle}

// #7b7c7d
// #6ca1a2